/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vivero.beans;

import java.util.ArrayList;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import vivero.clases.Clima;
import vivero.clases.Climas;
import vivero.clases.numAleatorios;

/**
 *
 * @author Liliana
 */
@Singleton
public class Estadosvivero implements EstadosviveroLocal {

    private Clima clima = new Clima();
    private numAleatorios num = new numAleatorios();
    private String estadonuevo;
    private double temperatura;
    private double val;
    

    @Lock(LockType.READ)
    @Override
    public String obtenerEstadoClima(double temp) { //Esta no es una funcion que se use externamente
        val = num.getTemperatura();
        temperatura = temp + val;
        clima.temperatura(temperatura);
        return clima.getClima().toString();
    }
    
    @Lock(LockType.READ)
    @Override
    public int obtenernuevatemperatura() { //Esto no sirve
        int temperaturan = num.generarAleatorios(5, 20);
        return temperaturan;
    }
    
    
    @Lock(LockType.READ)
    @Override
    public ArrayList nuevoEstado(String estadoactual, double tempe) {// el estado actual debe estar almacenado en la aplicacion
        ArrayList temp = new ArrayList();
        setEstadonuevo(obtenerEstadoClima(tempe));
        temp.add(temperatura);
        temp.add(getEstadonuevo());
       return temp;
    }

    public String getEstadonuevo() {
        return estadonuevo;
    }

    public void setEstadonuevo(String estadonuevo) {
        this.estadonuevo = estadonuevo;
    }

}
