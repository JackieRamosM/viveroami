/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vivero.beans;

import java.util.ArrayList;
import javax.ejb.Local;
import vivero.clases.Climas;

/**
 *
 * @author Liliana
 */
@Local
public interface EstadosviveroLocal {

    public String obtenerEstadoClima(double temp);

    public ArrayList nuevoEstado(String estadoactual, double tempe);

    public int obtenernuevatemperatura();

}
