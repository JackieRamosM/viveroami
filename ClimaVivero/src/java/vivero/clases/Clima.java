/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vivero.clases;

/**
 *
 * @author Liliana
 */


public class Clima {

    private Climas clima;

    public void temperatura(double temp) {

        if (temp < 2) {
            setClima(Climas.HELADO);
        }

        if (temp >= 2 && temp < 10) {
            setClima(Climas.FRIO);
        }

        if (temp >= 10 && temp < 20) {
            setClima(Climas.FRESCO);
        }

        if (temp >= 20 && temp < 30) {
            setClima(Climas.TEMPLADO);
        }

        if (temp >= 30 && temp < 40) {
            setClima(Climas.CALUROSO);
        }

        if (temp >= 40) {
            setClima(Climas.MUYCALUROSO);
        }
    }

    public Climas getClima() {
        return clima;
    }

    public void setClima(Climas clima) {
        this.clima = clima;
    }
}
