/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vivero.clases;

import java.util.Random;

/**
 *
 * @author Liliana
 */
public class numAleatorios {

    private int temperatura;
    private int humedad;

    public int generarAleatorios(int min, int max) {
        int num = -min + (int) (Math.random() * ((max - (-min)) + 1));
        return num;
    }

    public int getTemperatura() {
        temperatura = generarAleatorios(3, 3);
        return temperatura;
    }

    public int getHumedad() {
        humedad = generarAleatorios(0, 100);
        return humedad;
    }
}
