/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vivero.service;

import java.util.ArrayList;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import vivero.beans.EstadosviveroLocal;

/**
 *
 * @author Liliana
 */
@WebService(serviceName = "Viveroservice")
public class Viveroservice {
    @EJB
    private EstadosviveroLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "obtenerEstadoClima")
    public String obtenerEstadoClima(@WebParam(name = "temp") double temp) {
        return ejbRef.obtenerEstadoClima(temp);
    }

    @WebMethod(operationName = "nuevoEstado")
    public ArrayList nuevoEstado(@WebParam(name = "estadoactual") String estadoactual, @WebParam(name = "tempe") double tempe) {
        return ejbRef.nuevoEstado(estadoactual, tempe);
    }

    @WebMethod(operationName = "obtenernuevatemperatura")
    public int obtenernuevatemperatura() {
        return ejbRef.obtenernuevatemperatura();
    }
    
}
