CREATE DATABASE  IF NOT EXISTS `viveroami` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `viveroami`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: viveroami
-- ------------------------------------------------------
-- Server version	5.7.5-m15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `controles`
--

DROP TABLE IF EXISTS `controles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controles` (
  `idcontroles` int(11) NOT NULL AUTO_INCREMENT,
  `estadofoco` varchar(3) NOT NULL,
  `estadoventilador` varchar(3) NOT NULL,
  `hora` timestamp(5) NOT NULL DEFAULT CURRENT_TIMESTAMP(5) ON UPDATE CURRENT_TIMESTAMP(5),
  PRIMARY KEY (`idcontroles`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controles`
--

LOCK TABLES `controles` WRITE;
/*!40000 ALTER TABLE `controles` DISABLE KEYS */;
/*!40000 ALTER TABLE `controles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temperatura`
--

DROP TABLE IF EXISTS `temperatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temperatura` (
  `idtemperatura` int(11) NOT NULL AUTO_INCREMENT,
  `temperatura` varchar(10) NOT NULL,
  `fecha` varchar(30) NOT NULL,
  `estadoclima` varchar(45) NOT NULL,
  `humedad` varchar(10) NOT NULL,
  PRIMARY KEY (`idtemperatura`)
) ENGINE=InnoDB AUTO_INCREMENT=308 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temperatura`
--

LOCK TABLES `temperatura` WRITE;
/*!40000 ALTER TABLE `temperatura` DISABLE KEYS */;
INSERT INTO `temperatura` VALUES (1,'-10','2015-01-13 21:15:30','HELADO','30'),(291,'0.00','2015-01-25 20:29:04','HELADO',' '),(292,'4.00','2015-01-25 20:30:04','FRIO',' '),(293,'2.00','2015-01-25 20:31:05','FRIO',' '),(294,'0.00','2015-01-25 20:32:06','HELADO',' '),(295,'8.00','2015-01-25 20:33:07','FRIO',' '),(296,'8.00','2015-01-25 20:34:07','FRIO',' '),(297,'8.00','2015-01-25 20:38:13','FRIO',' '),(298,'12.00','2015-01-25 20:39:14','FRESCO',' '),(299,'14.00','2015-01-25 20:40:14','FRESCO',' '),(300,'16.00','2015-01-25 20:43:01','FRESCO',' '),(301,'16.00','2015-01-25 20:44:01','FRESCO',' '),(302,'16.00','2015-01-25 20:45:02','FRESCO',' '),(303,'20.00','2015-01-25 20:46:02','TEMPLADO',' '),(304,'-10.0','2015-01-25 20:47:03','HELADO',' '),(306,'0.00','2015-01-25 20:50:57','HELADO',' '),(307,'21','2015-01-25 20:51:57','TEMPLADO',' ');
/*!40000 ALTER TABLE `temperatura` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-25 23:03:10
