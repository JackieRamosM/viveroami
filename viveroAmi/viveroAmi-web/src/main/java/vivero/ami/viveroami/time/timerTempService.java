/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vivero.ami.viveroami.time;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import vivero.ami.entities.Temperatura;
import vivero.ami.viveroami.web.Humedad;
import vivero.ami.viveroami.web.MyVaadinUI;

/**
 *
 * @author Jimmy
 */
public class timerTempService implements Runnable, Serializable {

    private Temperatura temp = new Temperatura();
    private Humedad humedad = new Humedad();
    private double nhumedad;
    private String estadoVivero;
    private double tempVivero;
    private List climaExterno;
    private MyVaadinUI ui;
    private int i;
    private double efectoLuz;
    private double efectoVent;
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DecimalFormat df1 = new DecimalFormat("#,###,###,##0.00");

    private TimeZone timeZone;
    private Calendar c;

    public timerTempService(MyVaadinUI ui) {

        this.climaExterno = new ArrayList();
        this.climaExterno.add(10);
        this.climaExterno.add("FRESCO");
        this.ui = ui;
        this.tempVivero = Double.parseDouble(ui.getNtemperatura());
        this.nhumedad = Double.parseDouble(ui.getHumedad());
        this.estadoVivero = ui.getEstado();
        this.i = 1;
        this.timeZone = TimeZone.getTimeZone("GMT-5");

    }

    @Override
    public void run() {
        while (true) {
            // Se setea en los componentes necesarios el primer clima y la temperatura a variar
            ui.access(new Runnable() {

                @Override
                public void run() {
                    ui.getCurrentTempLabel().setValue(estadoVivero);
                    ui.getCurrentNumbLabel().setValue(df1.format(tempVivero) + "°C");
                    ui.getCurrentNumbE().setValue(climaExterno.get(0)+"°C");
                    ui.getCurrentTempE().setValue(""+climaExterno.get(1));
                    ui.getTempE().setValue("Temperatura Externa: "+climaExterno.get(1)+" - "+climaExterno.get(0)+"°C");
                    ui.getTempV().setValue("Temperatura Vivero: "+estadoVivero+" - "+df1.format(tempVivero) + "°C");
                    //ui.getCurrentNumbLabel().setValue(df1.format(nhumedad) + "%");
                    actualizarPlanta();
                    actualizarTemp();
                }
            });

            if (i % 5 == 0) {
                confControles();
                c = Calendar.getInstance(timeZone);
                ui.addTempToContainer(c, tempVivero);
                if (nhumedad > 100) {
                    nhumedad = 100;
                }
                if (nhumedad < 0) {
                    nhumedad = 0;
                }
                ui.addHumToContainer(c, nhumedad);
                System.out.println(climaExterno + " " + tempVivero + " " + estadoVivero + " " + nhumedad);
                tempVivero = efectoLuz + efectoVent + tempVivero;
                if (Double.parseDouble(climaExterno.get(0).toString()) > tempVivero) {
                    tempVivero++;
                    estadoVivero = estado(tempVivero);
                } else {
                    tempVivero--;
                    estadoVivero = estado(tempVivero);
                }
                nhumedad = humedad.getHumedad(tempVivero, Double.parseDouble(climaExterno.get(0).toString()));
            }

            if (i % 28 == 0) {
                nuevoclima();
            }

            if (i % 60 == 0) {
                //guardarTemp();
                
            }

            i++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }
    
    public void actualizarPlanta(){
        if (tempVivero < (ui.getUmbralInferior()-15.0)) {
            ui.getImagePlanta().setSource(ui.getPlantaHeladoRes());         
        }
        if (tempVivero < ui.getUmbralInferior()) {
            ui.getImagePlanta().setSource(ui.getPlantaFrioRes());         
        }
        if (tempVivero >= ui.getUmbralInferior() && tempVivero <= ui.getUmbralSuperior()) {
            ui.getImagePlanta().setSource(ui.getPlantaTempladoRes());
        }
        if (tempVivero > ui.getUmbralSuperior()) {
            ui.getImagePlanta().setSource(ui.getPlantaCalurosoRes());
        }
        if (tempVivero > (ui.getUmbralSuperior()+15.0)) {
            ui.getImagePlanta().setSource(ui.getPlantaMuycalurosoRes());
        }
    }
    
    public void actualizarTemp() {

        if (tempVivero < 2) {
            ui.getImagePlanta().setSource(ui.getPlantaHeladoRes());
            ui.getCurrentNumbLabel().setStyleName("labelTempNumb helado");
            ui.getCurrentTempLabel().setStyleName("labelTempDesc helado");
            ui.getCurrentNumbE().setStyleName("labelTempNumb helado");
            ui.getCurrentTempE().setStyleName("labelTempDesc helado");
            ui.getTempE().setStyleName("labelTempNumb helado");
            ui.getTempV().setStyleName("labelTempNumb helado");
        }

        if (tempVivero >= 2 && tempVivero < 10) {
            ui.getImagePlanta().setSource(ui.getPlantaFrioRes());
            ui.getCurrentNumbLabel().setStyleName("labelTempNumb frio");
            ui.getCurrentTempLabel().setStyleName("labelTempDesc frio");
            ui.getCurrentNumbE().setStyleName("labelTempNumb frio");
            ui.getCurrentTempE().setStyleName("labelTempDesc frio");
            ui.getTempE().setStyleName("labelTempNumb frio");
            ui.getTempV().setStyleName("labelTempNumb frio");
        }

        if (tempVivero >= 10 && tempVivero < 20) {
            ui.getImagePlanta().setSource(ui.getPlantaFrescoRes());
            ui.getCurrentNumbLabel().setStyleName("labelTempNumb fresco");
            ui.getCurrentTempLabel().setStyleName("labelTempDesc fresco");
            ui.getCurrentNumbE().setStyleName("labelTempNumb fresco");
            ui.getCurrentTempE().setStyleName("labelTempDesc fresco");
            ui.getTempE().setStyleName("labelTempNumb fresco");
            ui.getTempV().setStyleName("labelTempNumb fresco");
        }

        if (tempVivero >= 20 && tempVivero < 30) {
            ui.getImagePlanta().setSource(ui.getPlantaTempladoRes());
            ui.getCurrentNumbLabel().setStyleName("labelTempNumb templado");
            ui.getCurrentTempLabel().setStyleName("labelTempDesc tenplado");
            ui.getCurrentNumbE().setStyleName("labelTempNumb tenplado");
            ui.getCurrentTempE().setStyleName("labelTempDesc tenplado");
            ui.getTempE().setStyleName("labelTempNumb tenplado");
            ui.getTempV().setStyleName("labelTempNumb tenplado");
        }

        if (tempVivero >= 30 && tempVivero < 40) {
            ui.getImagePlanta().setSource(ui.getPlantaCalurosoRes());
            ui.getCurrentNumbLabel().setStyleName("labelTempNumb caluroso");
            ui.getCurrentTempLabel().setStyleName("labelTempDesc caluroso");
            ui.getCurrentNumbE().setStyleName("labelTempNumb caluroso");
            ui.getCurrentTempE().setStyleName("labelTempDesc caluroso");
            ui.getTempE().setStyleName("labelTempNumb caluroso");
            ui.getTempV().setStyleName("labelTempNumb caluroso");
        }

        if (tempVivero >= 40) {
            ui.getImagePlanta().setSource(ui.getPlantaMuycalurosoRes());
            ui.getCurrentNumbLabel().setStyleName("labelTempNumb muycaluroso");
            ui.getCurrentTempLabel().setStyleName("labelTempDesc muycaluroso");
            ui.getCurrentNumbE().setStyleName("labelTempNumb muycaluroso");
            ui.getCurrentTempE().setStyleName("labelTempDesc muycaluroso");
            ui.getTempE().setStyleName("labelTempNumb muycaluroso");
            ui.getTempV().setStyleName("labelTempNumb muycaluroso");
        }

    }

    public void confControles() {
        switch (ui.getControladores().getPotVent()) {
            case 0:
                efectoVent = 0;
                break;
            case 1:
                efectoVent = -1;
                break;
            case 2:
                efectoVent = -2;
                break;
            case 3:
                efectoVent = -3;
                break;
        }
        switch (ui.getControladores().getPotLuz()) {
            case 0:
                efectoLuz = 0;
                break;
            case 1:
                efectoLuz = 1;
                break;
            case 2:
                efectoLuz = 2;
                break;
            case 3:
                efectoLuz = 3;
                break;
        }
    }

    public void guardarTemp() {
        Date date = new Date();
        temp.setIdtemperatura(Integer.SIZE);
        temp.setEstadoclima(estadoVivero);
        temp.setFecha(dateFormat.format(date));
        temp.setHumedad("" + df1.format(nhumedad));
        temp.setTemperatura("" + df1.format(tempVivero));
        System.out.println(df1.format(nhumedad));
        ui.guardarTempactual(temp);
    }

    public void nuevoclima() {
        
        try { // Call Web Service Operation
            vivero.service.Viveroservice_Service service = new vivero.service.Viveroservice_Service();
            vivero.service.Viveroservice port = service.getViveroservicePort();
            // TODO initialize WS operation arguments here
            java.lang.String estadoactual = climaExterno.get(1).toString();
            double tempe = Double.parseDouble(climaExterno.get(0).toString());
            // TODO process result here
            java.util.List<java.lang.Object> result = port.nuevoEstado(estadoactual, tempe);
            climaExterno = result;
        } catch (Exception ex) {
            // TODO handle custom exceptions here
        }

    }
    
            
   
    //

    public String estado(double tmp) {
        String estado = "";
        if (tmp < 2) {
            estado = "HELADO";
        }

        if (tmp >= 2 && tmp < 10) {
            estado = "FRIO";
        }

        if (tmp >= 10 && tmp < 20) {
            estado = "FRESCO";
        }

        if (tmp >= 20 && tmp < 30) {
            estado = "TEMPLADO";
        }

        if (tmp >= 30 && tmp < 40) {
            estado = "CALUROSO";
        }

        if (tmp >= 40) {
            estado = "MUY CALUROSO";
        }
        return estado;
    }
}
