/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vivero.ami.viveroami.web;

import java.io.Serializable;

/**
 *
 * @author Liliana
 */
public class Humedad implements Serializable {

    double humedad = 0;

    public double getHumedad(double tempVivero, double tempExterno) {
        
        humedad = 100 * Math.pow(((tempExterno + 110) / (110 + tempVivero)), 8);
        return humedad;
    }

}
