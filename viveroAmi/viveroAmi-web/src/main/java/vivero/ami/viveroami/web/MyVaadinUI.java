package vivero.ami.viveroami.web;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.Axis;
import com.vaadin.addon.charts.model.AxisType;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.DataSeriesItem;
import com.vaadin.addon.charts.model.HorizontalAlign;
import com.vaadin.addon.charts.model.Labels;
import com.vaadin.addon.charts.model.Legend;
import com.vaadin.addon.charts.model.PlotBandLabel;
import com.vaadin.addon.charts.model.PlotLine;
import com.vaadin.addon.charts.model.PlotOptionsSeries;
import com.vaadin.addon.charts.model.Title;
import com.vaadin.addon.charts.model.XAxis;
import com.vaadin.addon.charts.model.YAxis;
import com.vaadin.addon.charts.model.style.SolidColor;
import com.vaadin.addon.charts.model.style.Style;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.cdi.CDIUI;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import vivero.ami.entities.Temperatura;
import vivero.ami.facade.TemperaturaFacadeLocal;
import vivero.ami.viveroami.control.TempControl;
import vivero.ami.viveroami.time.timerTempService;

@SuppressWarnings("serial")
@CDIUI("")
@Theme("mytheme")
public class MyVaadinUI extends UI implements Serializable {

    @Inject
    private TemperaturaFacadeLocal temperaturafacade;
    private Temperatura temperaturaactual;
    private String estado;
    private String humedad;
    private String ntemperatura = "";
    private String fecha;
    private List<Temperatura> temperaturas = new ArrayList<Temperatura>();
    private TempControl controladores = new TempControl();
    private Calendar cal = Calendar.getInstance();
    private SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private timerTempService timeThread;
    private Double UmbralSuperior=30.0, UmbralInferior=20.0;
    
    //Componentes UI variables
    private Label tempE;
    private Label tempV;
    private Label currentTempLabel;
    private Label currentNumbLabel;
    private Label currentTempE;
    private Label currentNumbE;
    private Image imagePlanta;
    private Image imageVentilador;
    private Image imageFoco;
    private Chart dayTempChart;
    private Chart actTempChart;
    private Chart actHumChart;
    private Table tempTable;
    private DataSeries contenedorTempActuales;
    private DataSeries contenedorTempDay;
    private DataSeries contenedorHumActuales;
    private Notification notificacion;
    private PlotLine umbralS;
    private PlotLine umbralI;
    private PlotBandLabel lblUmbralS;
    private PlotBandLabel lblUmbralI;
    private InlineDateField datePickr;
    
    //Resources
    private String basepath;
    private FileResource titleRes;
    private FileResource plantaCalurosoRes;
    private FileResource plantaFrescoRes;
    private FileResource plantaFrioRes;
    private FileResource plantaHeladoRes;
    private FileResource plantaMuycalurosoRes;
    private FileResource plantaTempladoRes;
    private FileResource ventApagadoRes;
    private FileResource ventP1Res;
    private FileResource ventP2Res;
    private FileResource ventP3Res;
    private FileResource focoApagadoRes;
    private FileResource focoP1Res;
    private FileResource focoP2Res;
    private FileResource focoP3Res;
    
    //Configuracion
    private TextField uInfTxt;
    private TextField uSupTxt;
    
    @WebServlet(value = "/*", asyncSupported = true, initParams = {
        @WebInitParam(
                name = "session-timeout",
                value = "60"
        ),
        @WebInitParam(
                name = "UIProvider",
                value = "com.vaadin.cdi.CDIUIProvider"
        )})
    @VaadinServletConfiguration(productionMode = false, ui = MyVaadinUI.class, widgetset = "vivero.ami.viveroami.web.AppWidgetSet")

    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {

        final HorizontalLayout layout = new HorizontalLayout();
        findtemperatura();
        layout.setMargin(true);
        layout.addStyleName("layoutBackground");
        layout.setSizeFull();
        layout.setDefaultComponentAlignment(Alignment.TOP_LEFT);
        setContent(layout);
        setPollInterval(1000);

        timeThread = new timerTempService(this);
        new Thread(timeThread).start();

        //Resources Init.
        basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
        titleRes = new FileResource(new File(basepath + "/WEB-INF/images/titl.png"));
        plantaCalurosoRes = new FileResource(new File(basepath + "/WEB-INF/images/plantacaluroso.png"));
        plantaFrescoRes = new FileResource(new File(basepath + "/WEB-INF/images/plantafresco.png"));
        plantaFrioRes = new FileResource(new File(basepath + "/WEB-INF/images/plantafrio.png"));
        plantaHeladoRes = new FileResource(new File(basepath + "/WEB-INF/images/plantahelado.png"));
        plantaMuycalurosoRes = new FileResource(new File(basepath + "/WEB-INF/images/plantamuycaluroso.png"));
        plantaTempladoRes = new FileResource(new File(basepath + "/WEB-INF/images/plantatemplad.png"));
        ventApagadoRes = new FileResource(new File(basepath + "/WEB-INF/images/ventiladorp1.png"));
        ventP1Res = new FileResource(new File(basepath + "/WEB-INF/images/ventiladorp1.png"));
        ventP2Res = new FileResource(new File(basepath + "/WEB-INF/images/ventiladorp2.png"));
        ventP3Res = new FileResource(new File(basepath + "/WEB-INF/images/ventiladorp3.png"));
        focoApagadoRes = new FileResource(new File(basepath + "/WEB-INF/images/focoapagado.png"));
        focoP1Res = new FileResource(new File(basepath + "/WEB-INF/images/focop1.png"));
        focoP2Res = new FileResource(new File(basepath + "/WEB-INF/images/focop2.png"));
        focoP3Res = new FileResource(new File(basepath + "/WEB-INF/images/focop3.png"));

        VerticalLayout mainLayout = new VerticalLayout();
        //mainLayout.setMargin(true);

        Image titleImg = new Image("", titleRes);
        mainLayout.addComponent(titleImg);
        mainLayout.getComponent(0).addStyleName("titulo");
        
        TabSheet menuTab = new TabSheet();
        menuTab.setHeight(650.0f, Unit.PIXELS);

        /*
         *** Ventana de visor de temperatura.
         */
        HorizontalLayout tempLayout = new HorizontalLayout();
        tempLayout.setMargin(true);
        tempLayout.setSizeFull();
        tempLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        menuTab.addTab(tempLayout, "Datos del Vivero");

        contenedorTempActuales = new DataSeries();
        contenedorHumActuales = new DataSeries();
        contenedorTempDay = new DataSeries();
        contenedorTempActuales.setName("Cambios de Temperatura");
        contenedorHumActuales.setName("Cambios de Humedad Relativa");
        contenedorTempDay.setName("Cambios de Temperatura");
        try {
            cal.setTime(sdf.parse(fecha));
            addTempToContainer(cal, Double.parseDouble(temperaturaactual.getTemperatura()));
            addTempToContainer(cal, Double.parseDouble(temperaturaactual.getHumedad()));
        } catch (Exception e) {
        }
        
        VerticalLayout chartL = new VerticalLayout();
        chartL.setMargin(false);
        chartL.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
        
        tempE = new Label("");
        tempE.setStyleName("labelTempDesc");
        tempE.setWidth("");
        
        tempV = new Label("");
        tempV.setStyleName("labelTempDesc");
        tempV.setWidth("");
        
        chartL.addComponent(getChartTemp());
        chartL.addComponent(tempE);
        chartL.addComponent(tempV);
        
        tempLayout.addComponent(chartL);
        tempLayout.addComponent(getChartHum());
        
        imagePlanta = new Image("", plantaTempladoRes);
        imagePlanta.setHeight("100%");
        tempLayout.addComponent(imagePlanta);

        /*
         *** Ventana de control de sensores de vivero.
         */
        HorizontalLayout contLayout = new HorizontalLayout();
        contLayout.setMargin(true);
        contLayout.setSizeFull();
        contLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        menuTab.addTab(contLayout, "Control de Sensores");

        VerticalLayout ventiladorLayout = new VerticalLayout();
        ventiladorLayout.setMargin(false);
        ventiladorLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        Label ventLabel = new Label("Ventilador");
        ventLabel.addStyleName("labelTitles");
        ventLabel.setWidth("");
        ventiladorLayout.addComponent(ventLabel);

        imageVentilador = new Image("", ventApagadoRes);
        imageVentilador.setHeight("200px");

        ventiladorLayout.addComponent(imageVentilador);

        HorizontalLayout ventButtonLayout = new HorizontalLayout();
        ventButtonLayout.setMargin(true);

        Button offVent = new Button("Off");
        offVent.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                controladores.setLevelPotVent(0);
                imageVentilador.setSource(ventApagadoRes);
            }
        });
        ventButtonLayout.addComponent(offVent);

        Button ventLvl1 = new Button("1");
        ventLvl1.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                controladores.setLevelPotVent(1);
                imageVentilador.setSource(ventP1Res);

            }
        });
        ventButtonLayout.addComponent(ventLvl1);

        Button ventLvl2 = new Button("2");
        ventLvl2.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                controladores.setLevelPotVent(2);
                imageVentilador.setSource(ventP2Res);
            }
        });
        ventButtonLayout.addComponent(ventLvl2);

        Button ventLvl3 = new Button("3");
        ventLvl3.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                controladores.setLevelPotVent(3);
                imageVentilador.setSource(ventP3Res);
            }
        });
        ventButtonLayout.addComponent(ventLvl3);

        ventiladorLayout.addComponent(ventButtonLayout);

        contLayout.addComponent(ventiladorLayout);

        VerticalLayout focoLayout = new VerticalLayout();
        focoLayout.setMargin(false);
        focoLayout.setDefaultComponentAlignment(Alignment.TOP_CENTER);

        Label focoLabel = new Label("Foco");
        focoLabel.addStyleName("labelTitles");
        focoLabel.setWidth("");
        focoLayout.addComponent(focoLabel);

        imageFoco = new Image("", focoApagadoRes);
        imageFoco.setHeight("200px");
        focoLayout.addComponent(imageFoco);

        HorizontalLayout focButtonLayout = new HorizontalLayout();
        focButtonLayout.setMargin(true);

        Button offLight = new Button("Off");
        offLight.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                controladores.setLevelPotLuz(0);
                imageFoco.setSource(focoApagadoRes);
            }
        });
        focButtonLayout.addComponent(offLight);

        Button lightLvl1 = new Button("1");
        lightLvl1.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                controladores.setLevelPotLuz(1);
                imageFoco.setSource(focoP1Res);
            }
        });
        focButtonLayout.addComponent(lightLvl1);

        Button lightLvl2 = new Button("2");
        lightLvl2.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                controladores.setLevelPotLuz(2);
                imageFoco.setSource(focoP2Res);
            }
        });
        focButtonLayout.addComponent(lightLvl2);

        Button lightLvl3 = new Button("3");
        lightLvl3.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                controladores.setLevelPotLuz(3);
                imageFoco.setSource(focoP3Res);
            }
        });
        focButtonLayout.addComponent(lightLvl3);

        focoLayout.addComponent(focButtonLayout);

        contLayout.addComponent(focoLayout);

        VerticalLayout tempcLayout = new VerticalLayout();
        tempcLayout.setMargin(false);
        tempcLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        
        Label tempELabel = new Label("Temperatura Externa");
        tempELabel.addStyleName("labelTitles");
        tempELabel.setWidth("");
        tempcLayout.addComponent(tempELabel);
        
        currentNumbE = new Label("");
        currentNumbE.setStyleName("labelTempNumb");
        currentNumbE.setWidth("");
        tempcLayout.addComponent(currentNumbE);

        currentTempE = new Label("");
        currentTempE.setStyleName("labelTempDesc");
        currentTempE.setWidth("");
        tempcLayout.addComponent(currentTempE);
        
        Label tempcLabel = new Label("Temperatura Interna");
        tempcLabel.addStyleName("labelTitles");
        tempcLabel.setWidth("");
        tempcLayout.addComponent(tempcLabel);

        currentNumbLabel = new Label("");
        currentNumbLabel.setStyleName("labelTempNumb");
        currentNumbLabel.setWidth("");
        tempcLayout.addComponent(currentNumbLabel);

        currentTempLabel = new Label("");
        currentTempLabel.setStyleName("labelTempDesc");
        currentTempLabel.setWidth("");
        tempcLayout.addComponent(currentTempLabel);

        contLayout.addComponent(tempcLayout);

        /*
         *** Ventana de estadisticas del día
         */
        HorizontalLayout estadLayout = new HorizontalLayout();
        estadLayout.setMargin(true);
        estadLayout.setSizeFull();
        estadLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        datePickr = new InlineDateField();
        datePickr.setCaption("Seleccionar Día:");
        datePickr.setValue(new Date());
        datePickr.setImmediate(true);
        datePickr.setTimeZone(TimeZone.getTimeZone("GMT-5"));
        datePickr.setLocale(Locale.US);
        datePickr.setResolution(Resolution.DAY);
        datePickr.setStyleName("labelTitles");
        
        datePickr.addValueChangeListener(new ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                addItemsToTable(datePickr.getValue());
                addItemsToContainer(datePickr.getValue());
            }
        });
        estadLayout.addComponent(getTableTemp());
        estadLayout.addComponent(datePickr);
        estadLayout.addComponent(getDayChartTemp());
        
        menuTab.addTab(estadLayout, "Estadística Diaria");
        /**
         * Ventana de Configuraciones
         **/
        
        VerticalLayout configLayout = new VerticalLayout();
        configLayout.setMargin(true);
        configLayout.setSizeFull();
        configLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        
        HorizontalLayout USLayout = new HorizontalLayout();
        USLayout.setMargin(true);
        
        Label uSupLbl = new Label("Umbral Superior :");
        uSupLbl.addStyleName("labelTitles");
        
        uSupTxt = new TextField();
        uSupTxt.setImmediate(true);
        uSupTxt.setInputPrompt(""+getUmbralSuperior());
        uSupTxt.setDescription("El umbral Inferior define la temperatura ideal máxima para la planta");
        
        USLayout.addComponent(uSupLbl);
        USLayout.addComponent(uSupTxt);
        
        HorizontalLayout UILayout = new HorizontalLayout();
        UILayout.setMargin(true);
        
        Label uInfLbl = new Label("Umbral Inferior :");
        uInfLbl.addStyleName("labelTitles");
        
        uInfTxt = new TextField();
        uInfTxt.setImmediate(true);
        uInfTxt.setInputPrompt(""+getUmbralInferior());
        uInfTxt.setDescription("El umbral Inferior define la temperatura ideal mínima para la planta");
        
        UILayout.addComponent(uInfLbl);
        UILayout.addComponent(uInfTxt);
        
        HorizontalLayout btnLayout = new HorizontalLayout();
        btnLayout.setMargin(true);
        notificacion = new Notification("Cambios Aplicados Exitosamente",
                "Umbral Superior e Inferior modificados");
        
        Button btnAplicar = new Button("Aplicar");
        btnAplicar.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                setUmbralSuperior(Double.parseDouble(uSupTxt.getValue()));
                setUmbralInferior(Double.parseDouble(uInfTxt.getValue()));
                actTempChart.drawChart();
                refreshConfigComponents();
                notificacion.show(Page.getCurrent());
            }
        });
        
        Button btnCancelar = new Button("Cancelar");
        btnCancelar.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                refreshConfigComponents();
            }
        });
        
        btnLayout.addComponent(btnAplicar);
        btnLayout.addComponent(btnCancelar);
        
        configLayout.addComponent(USLayout);
        configLayout.addComponent(UILayout);
        configLayout.addComponent(btnLayout);
        
        menuTab.addTab(configLayout, "Configuraciones");
        
        menuTab.getTab(0).setStyleName("mainTab");
        mainLayout.addComponent(menuTab);

        layout.addComponent(mainLayout);

        FileResource sideRes = new FileResource(new File(basepath + "/WEB-INF/images/side.png"));
        Image imageSide = new Image("", sideRes);

        //layout.addComponent(imageSide);
        layout.getComponent(0).addStyleName("mainStyle");
        //layout.getComponent(1).addStyleName("sideStyle");

    }

    public void guardarTempactual(Temperatura temp) {
        try {
            temperaturafacade.create(temp);
        } catch (EJBException e) {
            @SuppressWarnings("ThrowableResultIgnored")
            Exception cause = e.getCausedByException();
            if (cause instanceof ConstraintViolationException) {
                @SuppressWarnings("ThrowableResultIgnored")
                ConstraintViolationException cve = (ConstraintViolationException) e.getCausedByException();
                for (Iterator<ConstraintViolation<?>> it = cve.getConstraintViolations().iterator(); it.hasNext();) {
                    ConstraintViolation<? extends Object> v = it.next();
                    System.err.println(v);
                    System.err.println("==>>" + v.getMessage());
                }
            }
        } catch (Exception e) {
        }
    }

    protected Component getChartTemp() {
        actTempChart = new Chart(ChartType.SCATTER);
        Configuration conf = actTempChart.getConfiguration();
        conf.setTitle(new Title("Cambios de temperatura Actuales"));

        Legend legend = conf.getLegend();
        legend.setEnabled(true);
        conf.setExporting(false);
        conf.getChart().setAnimation(Boolean.TRUE);
        
        PlotOptionsSeries plotOptions = new PlotOptionsSeries();
        plotOptions.setLineWidth(1);
        conf.setPlotOptions(plotOptions);
        
        Axis xAxis = conf.getxAxis();
        //xAxis.setMinPadding(0.2);
        //xAxis.setMaxPadding(0.2);
        xAxis.setType(AxisType.DATETIME);

        YAxis yaxis = conf.getyAxis();
        // yaxis.setMin(-10);
        // yaxis.setMax(30);
       // yaxis.setMinPadding(0.2);
        //yaxis.setMaxPadding(0.2);
        yaxis.setTitle("Temperaturas (°C)");
        
        umbralS = new PlotLine();
        umbralS.setColor(new SolidColor("red"));
        umbralS.setValue(getUmbralSuperior());
        umbralS.setWidth(2);
        lblUmbralS = new PlotBandLabel("Temperatura Máxima Ideal: "+getUmbralSuperior()+"°C");
        lblUmbralS.setAlign(HorizontalAlign.CENTER);
        Style styleUS = new Style();
        styleUS.setColor(new SolidColor("red"));
        lblUmbralS.setStyle(styleUS);
        umbralS.setLabel(lblUmbralS);
        
        umbralI = new PlotLine();
        umbralI.setColor(new SolidColor("skyblue"));
        umbralI.setValue(getUmbralInferior());
        umbralI.setWidth(2);
        lblUmbralI = new PlotBandLabel("Temperatura Mínima Ideal: "+getUmbralInferior()+"°C");
        lblUmbralI.setAlign(HorizontalAlign.CENTER);
        Style styleUI = new Style();
        styleUI.setColor(new SolidColor("skyblue"));
        lblUmbralI.setStyle(styleUI);
        umbralI.setLabel(lblUmbralI);
        
        yaxis.setPlotLines(umbralS,umbralI);
        
        
        conf.getTooltip().setxDateFormat("%d.%m. %Y %H:%M");
        conf.getTooltip().setFormatter("''+ this.series.name +': '+ this.y +'°C'");
        
        
        conf.addSeries(contenedorTempActuales);
        //conf.addSeries(contenedorHumActuales);
        //conf.addSeries(umbralInferior);
        //conf.addSeries(umbralSuperior);
        actTempChart.drawChart(conf);

        return actTempChart;
    }
    
    protected Component getDayChartTemp() {
        dayTempChart = new Chart(ChartType.SCATTER);
        Configuration conf = dayTempChart.getConfiguration();
        conf.setTitle(new Title("Cambios de temperatura del día seleccionado"));

        Legend legend = conf.getLegend();
        legend.setEnabled(true);
        conf.setExporting(false);
        conf.getChart().setAnimation(Boolean.TRUE);
        
        PlotOptionsSeries plotOptions = new PlotOptionsSeries();
        plotOptions.setLineWidth(1);
        conf.setPlotOptions(plotOptions);
        
        Axis xAxis = conf.getxAxis();
        //xAxis.setMinPadding(0.2);
        //xAxis.setMaxPadding(0.2);
        xAxis.setType(AxisType.DATETIME);

        YAxis yaxis = conf.getyAxis();
        // yaxis.setMin(-10);
        // yaxis.setMax(30);
       // yaxis.setMinPadding(0.2);
        //yaxis.setMaxPadding(0.2);
        yaxis.setTitle("Temperaturas (°C)");
        
        PlotLine umbralSup = new PlotLine();
        umbralSup.setColor(new SolidColor("red"));
        umbralSup.setValue(getUmbralSuperior());
        umbralSup.setWidth(2);
        PlotBandLabel lblUmbralSup = new PlotBandLabel("Temperatura Máxima Ideal: "+getUmbralSuperior()+"°C");
        lblUmbralSup.setAlign(HorizontalAlign.CENTER);
        Style styleUS = new Style();
        styleUS.setColor(new SolidColor("red"));
        lblUmbralSup.setStyle(styleUS);
        umbralSup.setLabel(lblUmbralSup);
        
        PlotLine umbralInf = new PlotLine();
        umbralInf.setColor(new SolidColor("skyblue"));
        umbralInf.setValue(getUmbralInferior());
        umbralInf.setWidth(2);
        PlotBandLabel lblUmbralInf = new PlotBandLabel("Temperatura Mínima Ideal: "+getUmbralInferior()+"°C");
        lblUmbralInf.setAlign(HorizontalAlign.CENTER);
        Style styleUI = new Style();
        styleUI.setColor(new SolidColor("skyblue"));
        lblUmbralInf.setStyle(styleUI);
        umbralInf.setLabel(lblUmbralInf);
        
        yaxis.setPlotLines(umbralSup,umbralInf);
        
        
        conf.getTooltip().setxDateFormat("%d.%m. %Y %H:%M");
        conf.getTooltip().setFormatter("''+ this.series.name +': '+ this.y +'°C'");
        
        addItemsToContainer(datePickr.getValue());
        
        conf.addSeries(contenedorTempDay);

        dayTempChart.drawChart(conf);

        return dayTempChart;
    }
    
    public void addItemsToContainer(Date date){
        temperaturas = temperaturafacade.findAll();
        dayTempChart.drawChart();
        contenedorTempDay.clear();
        int i=0;
        for (Temperatura t : temperaturas ){
            try {
                Date rowDate = df.parse(t.getFecha());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                if(sdf.format(rowDate).equals(sdf.format(date)))
                    contenedorTempDay.add(new DataSeriesItem(rowDate, Double.parseDouble(t.getTemperatura())));
                i++;
            } catch (ParseException ex) {
                Logger.getLogger(MyVaadinUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    protected Component getChartHum() {
        actHumChart = new Chart(ChartType.SCATTER);
        Configuration conf = actHumChart.getConfiguration();
        conf.setTitle(new Title("Cambios de Humedad Actuales"));

        Legend legend = conf.getLegend();
        legend.setEnabled(true);
        conf.setExporting(false);

        PlotOptionsSeries plotOptions = new PlotOptionsSeries();
        plotOptions.setLineWidth(1);
        conf.setPlotOptions(plotOptions);

        conf.getChart().setAnimation(Boolean.TRUE);

        Axis xAxis = conf.getxAxis();
        //xAxis.setMinPadding(0.2);
        //xAxis.setMaxPadding(0.2);
        xAxis.setType(AxisType.DATETIME);

        YAxis yaxis = conf.getyAxis();
         yaxis.setMin(0);
         yaxis.setMax(100);
       // yaxis.setMinPadding(0.2);
        //yaxis.setMaxPadding(0.2);
        yaxis.setTitle("Humedad (%)");

        conf.getTooltip().setxDateFormat("%d.%m. %Y %H:%M");

        
        conf.addSeries(contenedorHumActuales);
        actHumChart.drawChart(conf);

        return actHumChart;
    }
    
    protected Component getTableTemp(){
        
        tempTable = new Table("Estados de Temperatura del Día");
        tempTable.setStyleName("tableStyle");
        tempTable.setHeight("400px");
        tempTable.addContainerProperty("Fecha", String.class, null);
        tempTable.addContainerProperty("Estado Clima", String.class, null);
        tempTable.addContainerProperty("Temperatura", String.class, null);
        tempTable.addContainerProperty("Humedad", String.class, null);
        
        addItemsToTable(datePickr.getValue());
        return tempTable;
    }
    
    
    
    public void addItemsToTable(Date date){
        temperaturas = temperaturafacade.findAll();
        tempTable.removeAllItems();
        int i=0;
        for (Temperatura t : temperaturas ){
            try {
                Date rowDate = df.parse(t.getFecha());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                if(sdf.format(rowDate).equals(sdf.format(date)))
                    tempTable.addItem(new Object[]{t.getFecha(),t.getEstadoclima(),t.getTemperatura(),t.getHumedad()},i);
                i++;
            } catch (ParseException ex) {
                Logger.getLogger(MyVaadinUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void findtemperatura() {
        //llamar a todas las temp de la base
        this.temperaturas = temperaturafacade.findAll();
        this.temperaturaactual = temperaturas.get(temperaturas.size() - 1);

        //inicializar temperatura actual
        this.estado = temperaturaactual.getEstadoclima();
        this.humedad = temperaturaactual.getHumedad();
        this.fecha = temperaturaactual.getFecha();
        this.ntemperatura = temperaturaactual.getTemperatura();
    }

    public Label getTempE() {
        return tempE;
    }

    public Label getTempV() {
        return tempV;
    }

    public Label getCurrentTempE() {
        return currentTempE;
    }

    public Label getCurrentNumbE() {
        return currentNumbE;
    }
    
    
    public void addTempToContainer(Calendar hora, double temperatura) {
        contenedorTempActuales.add(new DataSeriesItem(hora.getTimeInMillis(), temperatura));
    }

    public void addHumToContainer(Calendar hora, double humedad) {
        contenedorHumActuales.add(new DataSeriesItem(hora.getTimeInMillis(), humedad));
    }

    public void refreshConfigComponents(){
        uInfTxt.setValue("");
        uInfTxt.setInputPrompt(""+getUmbralInferior());
        uSupTxt.setValue("");
        uSupTxt.setInputPrompt(""+getUmbralSuperior());
    }

    public Label getCurrentTempLabel() {
        return currentTempLabel;
    }

    public Label getCurrentNumbLabel() {
        return currentNumbLabel;
    }

    public String getEstado() {
        return estado;
    }

    public String getHumedad() {
        return humedad;
    }

    public String getNtemperatura() {
        return ntemperatura;
    }

    public String getFecha() {
        return fecha;
    }

    public Chart getActTempChart() {
        return actTempChart;
    }

    public TempControl getControladores() {
        return controladores;
    }

    public Image getImagePlanta() {
        return imagePlanta;
    }

    public FileResource getPlantaCalurosoRes() {
        return plantaCalurosoRes;
    }

    public FileResource getPlantaFrescoRes() {
        return plantaFrescoRes;
    }

    public FileResource getPlantaFrioRes() {
        return plantaFrioRes;
    }

    public FileResource getPlantaHeladoRes() {
        return plantaHeladoRes;
    }

    public FileResource getPlantaMuycalurosoRes() {
        return plantaMuycalurosoRes;
    }

    public FileResource getPlantaTempladoRes() {
        return plantaTempladoRes;
    }

    public Double getUmbralSuperior() {
        return UmbralSuperior;
    }

    public void setUmbralSuperior(Double UmbralSuperior) {
        this.lblUmbralS.setText("Temperatura Máxima Ideal: "+UmbralSuperior+"°C");
        this.umbralS.setValue(UmbralSuperior);
        this.UmbralSuperior = UmbralSuperior;
    }

    public Double getUmbralInferior() {
        return UmbralInferior;
    }

    public void setUmbralInferior(Double UmbralInferior) {
        this.lblUmbralI.setText("Temperatura Máxima Ideal: "+UmbralInferior+"°C");
        this.umbralI.setValue(UmbralInferior);
        this.UmbralInferior = UmbralInferior;
    }
    
    
    
    
}
