/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vivero.ami.viveroami.control;

import java.io.Serializable;

/**
 *
 * @author Jimmy
 */
public class TempControl implements Serializable{
    private int potVent;
    private int potLuz;

    public TempControl() {
        this.potVent = 0;
        this.potLuz = 0;
    }
    
    public void setLevelPotLuz(int level){
        potLuz=level;
    }
    
    public void setLevelPotVent(int level){
        potVent=level;
    }

    public int getPotVent() {
        return potVent;
    }

    public int getPotLuz() {
        return potLuz;
    }
    
    
}
