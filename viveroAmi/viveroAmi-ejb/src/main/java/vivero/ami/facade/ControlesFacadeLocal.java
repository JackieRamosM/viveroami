/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vivero.ami.facade;

import java.util.List;
import javax.ejb.Local;
import vivero.ami.entities.Controles;

/**
 *
 * @author Jimmy
 */
@Local
public interface ControlesFacadeLocal {

    void create(Controles controles);

    void edit(Controles controles);

    void remove(Controles controles);

    Controles find(Object id);

    List<Controles> findAll();

    List<Controles> findRange(int[] range);

    int count();
    
}
