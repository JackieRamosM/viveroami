/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vivero.ami.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import vivero.ami.entities.Controles;

/**
 *
 * @author Jimmy
 */
@Stateless
public class ControlesFacade extends AbstractFacade<Controles> implements ControlesFacadeLocal {
    @PersistenceContext(unitName = "vivero.ami_viveroAmi-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ControlesFacade() {
        super(Controles.class);
    }
    
}
