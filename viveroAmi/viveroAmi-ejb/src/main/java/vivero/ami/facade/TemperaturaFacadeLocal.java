/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vivero.ami.facade;

import java.util.List;
import javax.ejb.Local;
import vivero.ami.entities.Temperatura;

/**
 *
 * @author Jimmy
 */
@Local
public interface TemperaturaFacadeLocal {

    void create(Temperatura temperatura);

    void edit(Temperatura temperatura);

    void remove(Temperatura temperatura);

    Temperatura find(Object id);

    List<Temperatura> findAll();

    List<Temperatura> findRange(int[] range);

    int count();
    
}
