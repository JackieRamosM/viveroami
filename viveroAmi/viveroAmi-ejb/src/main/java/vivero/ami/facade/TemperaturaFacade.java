/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vivero.ami.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import vivero.ami.entities.Temperatura;

/**
 *
 * @author Jimmy
 */
@Stateless
public class TemperaturaFacade extends AbstractFacade<Temperatura> implements TemperaturaFacadeLocal {
    @PersistenceContext(unitName = "vivero.ami_viveroAmi-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TemperaturaFacade() {
        super(Temperatura.class);
    }
    
}
