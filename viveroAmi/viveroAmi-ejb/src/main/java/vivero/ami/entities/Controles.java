/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vivero.ami.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jimmy
 */
@Entity
@Table(name = "controles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Controles.findAll", query = "SELECT c FROM Controles c"),
    @NamedQuery(name = "Controles.findByIdcontroles", query = "SELECT c FROM Controles c WHERE c.idcontroles = :idcontroles"),
    @NamedQuery(name = "Controles.findByEstadofoco", query = "SELECT c FROM Controles c WHERE c.estadofoco = :estadofoco"),
    @NamedQuery(name = "Controles.findByEstadoventilador", query = "SELECT c FROM Controles c WHERE c.estadoventilador = :estadoventilador"),
    @NamedQuery(name = "Controles.findByHora", query = "SELECT c FROM Controles c WHERE c.hora = :hora")})
public class Controles implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcontroles")
    private Integer idcontroles;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "estadofoco")
    private String estadofoco;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "estadoventilador")
    private String estadoventilador;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date hora;

    public Controles() {
    }

    public Controles(Integer idcontroles) {
        this.idcontroles = idcontroles;
    }

    public Controles(Integer idcontroles, String estadofoco, String estadoventilador, Date hora) {
        this.idcontroles = idcontroles;
        this.estadofoco = estadofoco;
        this.estadoventilador = estadoventilador;
        this.hora = hora;
    }

    public Integer getIdcontroles() {
        return idcontroles;
    }

    public void setIdcontroles(Integer idcontroles) {
        this.idcontroles = idcontroles;
    }

    public String getEstadofoco() {
        return estadofoco;
    }

    public void setEstadofoco(String estadofoco) {
        this.estadofoco = estadofoco;
    }

    public String getEstadoventilador() {
        return estadoventilador;
    }

    public void setEstadoventilador(String estadoventilador) {
        this.estadoventilador = estadoventilador;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcontroles != null ? idcontroles.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Controles)) {
            return false;
        }
        Controles other = (Controles) object;
        if ((this.idcontroles == null && other.idcontroles != null) || (this.idcontroles != null && !this.idcontroles.equals(other.idcontroles))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vivero.ami.entities.Controles[ idcontroles=" + idcontroles + " ]";
    }
    
}
