/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vivero.ami.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jimmy
 */
@Entity
@Table(name = "temperatura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Temperatura.findAll", query = "SELECT t FROM Temperatura t"),
    @NamedQuery(name = "Temperatura.findByIdtemperatura", query = "SELECT t FROM Temperatura t WHERE t.idtemperatura = :idtemperatura"),
    @NamedQuery(name = "Temperatura.findByTemperatura", query = "SELECT t FROM Temperatura t WHERE t.temperatura = :temperatura"),
    @NamedQuery(name = "Temperatura.findByFecha", query = "SELECT t FROM Temperatura t WHERE t.fecha = :fecha"),
    @NamedQuery(name = "Temperatura.findByEstadoclima", query = "SELECT t FROM Temperatura t WHERE t.estadoclima = :estadoclima"),
    @NamedQuery(name = "Temperatura.findByHumedad", query = "SELECT t FROM Temperatura t WHERE t.humedad = :humedad")})
public class Temperatura implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtemperatura")
    private Integer idtemperatura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "temperatura")
    private String temperatura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "fecha")
    private String fecha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "estadoclima")
    private String estadoclima;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "humedad")
    private String humedad;

    public Temperatura() {
    }

    public Temperatura(Integer idtemperatura) {
        this.idtemperatura = idtemperatura;
    }

    public Temperatura(Integer idtemperatura, String temperatura, String fecha, String estadoclima, String humedad) {
        this.idtemperatura = idtemperatura;
        this.temperatura = temperatura;
        this.fecha = fecha;
        this.estadoclima = estadoclima;
        this.humedad = humedad;
    }

    public Integer getIdtemperatura() {
        return idtemperatura;
    }

    public void setIdtemperatura(Integer idtemperatura) {
        this.idtemperatura = idtemperatura;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstadoclima() {
        return estadoclima;
    }

    public void setEstadoclima(String estadoclima) {
        this.estadoclima = estadoclima;
    }

    public String getHumedad() {
        return humedad;
    }

    public void setHumedad(String humedad) {
        this.humedad = humedad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtemperatura != null ? idtemperatura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Temperatura)) {
            return false;
        }
        Temperatura other = (Temperatura) object;
        if ((this.idtemperatura == null && other.idtemperatura != null) || (this.idtemperatura != null && !this.idtemperatura.equals(other.idtemperatura))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "vivero.ami.entities.Temperatura[ idtemperatura=" + idtemperatura + " ]";
    }
    
}
